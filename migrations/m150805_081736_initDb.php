<?php

use yii\db\Schema;
use yii\db\Migration;

class m150805_081736_initDb extends Migration
{
    public function safeUp()
    {
        $this->createTable('counterdata', [
            'rec_id' => Schema::TYPE_PK,
            'counter_id' => 'varchar(32) NOT NULL',
            'counter_data' => Schema::TYPE_DECIMAL . ' NOT NULL',
            'sender_ip' => Schema::TYPE_STRING . ' NOT NULL',
            'rec_ts' => Schema::TYPE_TIMESTAMP,
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    public function safeDown()
    {
        $this->dropTable('counterdata');
    }
}
