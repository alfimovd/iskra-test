<?php

namespace app\controllers;

use Yii;
use app\models\ContactForm;
use app\models\Counterdata;
use app\models\LoginForm;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new Counterdata();
        $dataProvider = new ActiveDataProvider([
            'query' => Counterdata::find(),
        ]);
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
        }
        $model = new Counterdata();
        return $this->render('index', [
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
    }
}
