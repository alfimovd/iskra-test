<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "counterdata".
 *
 * @property integer $rec_id
 * @property string $counter_id
 * @property string $counter_data
 * @property string $sender_ip
 * @property string $rec_ts
 */
class Counterdata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'counterdata';
    }

    public function behaviors()
    {
        return [
            'timestamps' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'rec_ts',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['counter_id', 'counter_data'], 'required'],
            [['counter_data'], 'number'],
            [['rec_ts'], 'safe'],
            [['counter_id'], 'string', 'max' => 32],
            [['sender_ip'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rec_id' => 'ID',
            'counter_id' => 'Прибор №',
            'counter_data' => 'Показание',
            'sender_ip' => 'Отправитель',
            'rec_ts' => 'Время',
        ];
    }

    private function getIP()
    {
        $this->sender_ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
    }

    private function sendSms()
    {
        file_get_contents("http://sms.ru/sms/send?api_id=bcd95e49-5602-b974-257d-abcd4e9deffd&to=79069150545&text=".urlencode(iconv("windows-1251","utf-8","In site new value!")));
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->getIP();
            if($this->counter_data == 3.1415926)
                $this->sendSms();
            return true;
        } else {
            return false;
        }
    }
}
