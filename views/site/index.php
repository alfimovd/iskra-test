<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Тестовое задание';
?>
<div class="site-index">

    <div class="jumbotron">
        <p class="lead">1. Написать одностраничное web-приложение обеспечивающее запись в БД MySQL

данных, введённых в текстовые поля и вывод последних 10 введённых записей. При 

условии ввода числа равного 3.1415926 в поле counter_data отправлять sms 

сообщение(использовать API сторонних систем).</p>

    </div>

    <div class="body-content">
        <div class="form">
            <h5>Добавить запись</h5>
            <?= $this->render('/counterdata/_form', [
                'model' => $model,
            ]) ?>
        </div>
        <div class="content">
            <h5>Добавленные записи</h5>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'rec_id',
                    'counter_id',
                    'counter_data',
                    'sender_ip',
                    'rec_ts',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
