<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Counterdata */

$this->title = 'Update Counterdata: ' . ' ' . $model->rec_id;
$this->params['breadcrumbs'][] = ['label' => 'Counterdatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->rec_id, 'url' => ['view', 'id' => $model->rec_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="counterdata-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
