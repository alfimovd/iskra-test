<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Counterdata */

$this->title = 'Create Counterdata';
$this->params['breadcrumbs'][] = ['label' => 'Counterdatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="counterdata-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
