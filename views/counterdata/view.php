<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Counterdata */

$this->title = $model->rec_id;
$this->params['breadcrumbs'][] = ['label' => 'Counterdatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="counterdata-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->rec_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->rec_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'rec_id',
            'counter_id',
            'counter_data',
            'sender_ip',
            'rec_ts',
        ],
    ]) ?>

</div>
