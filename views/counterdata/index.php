<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Counterdatas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="counterdata-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Counterdata', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'rec_id',
            'counter_id',
            'counter_data',
            'sender_ip',
            'rec_ts',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
