<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Counterdata */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="counterdata-form">

    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-lg-3">
	    	<?= $form->field($model, 'counter_id')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-lg-3">
	    	<?= $form->field($model, 'counter_data')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
